# Se me ha ido de las manos

Yo solo quería refrescar mis conocimientos de HTML y CSS de hace 20 años dándole mi toque a un CMS sencillo. Al final terminé modificando tanto el original que sentí que su licencia GPL me obligaba a publicarlo... y aquí estoy, aprendiendo ahora a usar gitlab sin yo ser nada de esto. 🤷🏻‍♂️

Disculpen todas las cosas que haya podido hacer mal, estilos desordenados o caos en las modificaciones. Es mi primerita vez.

# Mis modificaciones sobre el original

[**Portabloc**](https://portabloc.xyz/) es un CMS creado por [**Pabs**](https://gitlab.com/pabslow) con una programación bastante lógica y entendible desde fuera, lo que lo convierte en un proyecto ideal para trastear con él.

Para ejemplificar los cambios podéis visitar [**El Cuaderno**](https://cuaderno.jesusysustics.com/).
También he puesto una [**versión demo más pelada**](https://cloud.jesusysustics.com/test/demoportabloc/), directamente sacada de aquí y modificando únicamente el config.ini para añadir la dirección correspondiente.

A grandes rasgos, los cambios que he hecho son los siguientes:
- Cambios en el tema de colores, estilos de encabezados, textos, imágenes, sombras, texturas...
- Uso de fuentes libres desde [Bunny Fonts](https://fonts.bunny.net/), por aquello de la GDPR (siempre mejor autoalojarlas)
- Traducción al español de elementos como fechas y botones
- Descripción e imagen para opengraph, permitiendo una previsualización cuando se comparte en redes sociales.
- Fondo con colorinchis porque la web sencilla no tiene por qué ser blanca/negra.
- Botones con iconos en los menús en lugar de simples enlaces, etiquetas en formato botón, estilo diferente de los botones de seguir leyendo... 
- Iconos de licencia tanto GPL (del CMS) como Creative Commons (contenido del blog, eso ya cada cual).
- Mayor modularidad del blog mediante archivos independientes header (cabecera) y footer (pie de página). Ese tipo de fragmentación me produce serenidad.

Reitero que ni soy programador ni diseñador... solo soy un politólogo de carrera al que le divierte meterse en código porque es como resolver un puzle.

<details>Aunque la mayoría de las veces he utilizado recursos como [**MDN**](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity) para aprender cómo modificar los estilos, en ocasiones también he recurrido Google Gemini para resolver dudas o intentar que corrigiese mis errores de sintaxis.<summary>Disclaimer final sobre uso de Inteligencia Artificial</summary>
</details>


A continuación dejo el texto del proyecto original:

# Portabloc CMS

A tiny CMS for creative coders.

Create a beautiful blog just from plain text files. Drag and drop them to your /posts folder. Portabloc does the ~hard work~ magic for you 🔮🪄

* No databases, but your files and folders.
* Supports both Markdown and Gemini syntax.
* Tagging support
* Easy. Flexible. Portable.
* Lightweight framework
* Less than < 300 KiB

## Demo
↗️ Live demo: [portabloc.xyz](https://portabloc.xyz)

![Portabloc](https://portabloc.xyz/assets/placeholder-2.png)

![Portabloc](https://portabloc.xyz/assets/portabloc.jpg)
