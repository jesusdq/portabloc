
<?php if(config('show.tags') == true) { ?>
    <span class="tags-line">
        <?php if ($p->tags): foreach($p->tags as $tag){ if (!empty($tag)): echo '<a href="'.site_url().'tag/'.$tag.'" class="tag">#'.$tag.'</a>'; endif; } endif; ?>
    </span>
<?php } ?>

<br>
<br>

<?php 
    if (!empty($relatedtags[0])) {
       echo '<ul class="related">'; 
        foreach ($relatedtags as $relatedtag) {
            search_tag($relatedtag);
        }
        echo '</ul>';       
    }
?>