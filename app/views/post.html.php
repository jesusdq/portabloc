<div class="post single">

	<a href="<?php echo site_url(); ?>">◀ Inicio</a>
	<h2><?php echo $p->title ?></h2>

	<?php setlocale(LC_TIME, 'es_ES')?>
    <div class="date"><?php echo strftime("%d de %B del %Y", $p->date)?></div>
	<br>

	<?php echo $p->body?>

</div>
<div class="sidebar"> 
	<?php $relatedtags = $p->tags; ?>
	<?php include('related.html.php'); ?>

</div>

<!--Code highlighter -->
<?php if (config('snippet.highlight') == true): ?>
<link rel="stylesheet" href="<?php echo site_url() ?>assets/monokai.min.css">
<script src="<?php echo site_url() ?>assets/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
<?php endif; ?>
