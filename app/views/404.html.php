<!DOCTYPE html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes" />
	<title>404 Not Found | <?php echo config('blog.title') ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="icon" type="image/png" href="<?php echo site_url() ?>assets/notepad.png">
	<link rel="alternate" type="application/rss+xml" title="<?php echo config('blog.title')?>  Feed" href="<?php echo site_url()?>rss" />
	<link rel="stylesheet" media="screen" href="<?php echo site_url() ?>assets/new.css" async />
	<link rel="stylesheet" media="screen" href="<?php echo site_url() ?>assets/custom.css" async />
	<link rel="preconnect" href="https://fonts.bunny.net">
	<link href="https://fonts.bunny.net/css?family=outfit:300|righteous:400|ubuntu:300" rel="stylesheet" />
</head>
<body>
    <header>
		<?php include('header.html.php'); ?>
	</header>

	<content>
	<h2>Error 404: recurso no encontrado</h2>
	<br>
	<img src="<?php echo site_url(); ?>media/cat-in-a-box.png" alt="Un gato asoma dentro de una bolsa">
	<br>
	<p><a href="<?php echo site_url(); ?>">◀ <b>Volver a <?php echo config('blog.title')?></b></a></p>
	</content>

	<footer>
	    <?php include('footer.html.php'); ?>
    </footer>
</body>
</html>
