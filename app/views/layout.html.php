<!DOCTYPE html>
<html lang="es">
<head>
	<title><?php echo isset($title) ? _h($title) : config('blog.title') ?></title>

	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes" />
	<meta name="description" content="<?php echo config('blog.description')?>" />
	<link rel="icon" type="image/png" href="<?php echo site_url() ?>assets/favicon.png">
	<link rel="alternate" type="application/rss+xml" title="<?php echo config('blog.title')?>  Feed" href="<?php echo site_url()?>rss" />
	<link rel="stylesheet" media="screen" href="<?php echo site_url() ?>assets/new.css" async />
	<link rel="stylesheet" media="screen" href="<?php echo site_url() ?>assets/custom.css" async />
	<link rel="preconnect" href="https://fonts.bunny.net">
	<link href="https://fonts.bunny.net/css?family=outfit:300|righteous:400|ubuntu:300" rel="stylesheet" />

    <!-- Opengraph -->
    <meta property="og:url" content="<?php echo site_url() ?>">
    <meta property="og:type" content="website">
    <meta property="og:title" content="<?php echo isset($title) ? _h($title) : config('blog.title') ?>">
    <meta property="og:description" content="<?php echo config('blog.description')?>">
    <meta property="og:image" content="<?php echo site_url() ?>/assets/opengraph.png">
    <meta name="twitter:card" content="summary_large_image">
    <meta property="twitter:domain" content="<?php echo site_url() ?>">
    <meta property="twitter:url" content="<?php echo site_url() ?>">
    <meta name="twitter:title" content="<?php echo isset($title) ? _h($title) : config('blog.title') ?>">
    <meta name="twitter:description" content="<?php echo config('blog.description')?>">
    <meta name="twitter:image" content="<?php echo site_url() ?>/assets/opengraph.png">

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>
    <header>
		<?php include('header.html.php'); ?>
	</header>



	<section id="content">
		<?php echo content()?>
	</section>
	<footer>
	    <?php include('footer.html.php'); ?>
    </footer>
</body>
</html>